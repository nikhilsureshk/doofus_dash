using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstants  //Contstants class and Hence Not deriving from Mono Behaviour
{
    public const string DOOFUS_ANIMATOR_ID = "m_uCharacterState";
    public const string HORIZONTAL = "Horizontal";
    public const string VERTICAL = "Vertical";

    public const int PULPIT_OFFSET = 9;
    public enum DOOFUS_CHARACTER_STATE
    {
        NOT_SET,
        IDLE,
        RUNNING,
        CELEBRATING,
        DROP
    }
}



// one scene
[System.Serializable]
public class Player_Data
{
    public int speed;
}

[System.Serializable]
public class PulPit_Data
{
    public int min_pulpit_destroy_time;
    public int max_pulpit_destroy_time;
    public float pulpit_spawn_time;
}

[System.Serializable]
public class CongifurationData
{
    public Player_Data player_data;
    public PulPit_Data pulpit_data;
}
