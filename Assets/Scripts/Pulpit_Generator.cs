using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulpit_Generator : MonoBehaviour
{
    bool m_bGenerated = false;
    bool m_bDoofusExited = false;
    private GameObject currentGameObject;
   

    private GameLocals m_LocalsInstance; 
  
    void Start()
    {
        m_bGenerated = false;
        m_bDoofusExited = false;
        currentGameObject = this.gameObject;
        m_LocalsInstance = GameObject.Find("Game_UI").GetComponent<GameLocals>();
        Debug.Assert(m_LocalsInstance);
    }

    private void OnGUI()
    {
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if ((!m_bGenerated && !m_LocalsInstance.isGameOver()))
        {
            StartCoroutine("CreateNextPulpit");
            StartCoroutine("KillThisPupit");
            m_bGenerated = true;
        }
    }

    IEnumerator CreateNextPulpit()
    {
       
        yield return new WaitForSeconds(m_LocalsInstance.GetNewPulpitSpawnTime());

        List<KeyValuePair<float, float>> mPossiblePositionsList = ObtainNextValues();
        m_LocalsInstance.AddNewPulpit(ref currentGameObject, ref mPossiblePositionsList);       


    }
    IEnumerator KillThisPupit ()
    {
        yield return new WaitForSeconds(m_LocalsInstance.GetPulpitMinLifeTime());

        if (!m_bDoofusExited)
            yield return new WaitForSeconds(m_LocalsInstance.GetPulpitMaxLifeTime() - m_LocalsInstance.GetPulpitMinLifeTime());    
        Destroy(currentGameObject);
        
    }

    private void OnCollisionExit(Collision collision)
    {
        m_bDoofusExited = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Doofus")
            m_LocalsInstance.UpdateDoofusScore();            
    }

    private List<KeyValuePair<float, float>> ObtainNextValues()
    {
        List<KeyValuePair<float, float>> mPossiblePositionsList = new List<KeyValuePair<float, float>>();        
        int _positionCount = 0;       
      
        while (_positionCount < 4)
        {
            float _xCoord = currentGameObject.transform.position.x;
            float _zCoord = currentGameObject.transform.position.z;
            switch (_positionCount)
            {
                case 0:
                    _xCoord += GameConstants.PULPIT_OFFSET;
                    break;
                case 1:
                    _xCoord -= GameConstants.PULPIT_OFFSET;
                    break;
                case 2:
                    _zCoord -= GameConstants.PULPIT_OFFSET;
                    break;
                case 3:
                    _zCoord += GameConstants.PULPIT_OFFSET;
                    break;
            }
            
            mPossiblePositionsList.Add(new KeyValuePair<float, float>(_xCoord,_zCoord));
           _positionCount++;
        }
        return mPossiblePositionsList;

    }

}
