using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doofus_Controller : MonoBehaviour
{
    private Animator m_DoofusAnimator;
    private GameObject m_GMainCamera;  
    private GameLocals m_LocalsInstance;

    private int m_uDoofusCurrentState;

    Doofus_Controller()
    {
        m_uDoofusCurrentState = (int)GameConstants.DOOFUS_CHARACTER_STATE.NOT_SET;        
    }

   
    void Start()
    {
        m_DoofusAnimator = GetComponent<Animator>();

        m_GMainCamera = GameObject.Find("Main Camera");
        Debug.Assert(m_GMainCamera);

        m_LocalsInstance = GameObject.Find("Game_UI").GetComponent<GameLocals>();
        Debug.Assert(m_LocalsInstance);
        m_uDoofusCurrentState = (int)GameConstants.DOOFUS_CHARACTER_STATE.IDLE;
    }

  
    void FixedUpdate()
    {
        if (hasDoofusDropped())
        {
            StartCoroutine("DoofusDrop");            
        }
        else
            ApplyDoofusUpdate();
    }

    private void LateUpdate()
    {
        if (!hasDoofusDropped())
            m_GMainCamera.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 3.46f, this.transform.position.z - 8);
    }

    private bool hasDoofusDropped()
    {
        return (this.transform.position.y < 1.5f);
    }

    private void ApplyDoofusUpdate()
    {
        m_uDoofusCurrentState = (int)GameConstants.DOOFUS_CHARACTER_STATE.IDLE;
        if (Input.GetAxis(GameConstants.HORIZONTAL) != 0 || Input.GetAxis(GameConstants.VERTICAL) != 0)
        {
            m_uDoofusCurrentState = (int)GameConstants.DOOFUS_CHARACTER_STATE.RUNNING;
            Vector3 vTargetPos = this.transform.position;
            vTargetPos.x += Input.GetAxis(GameConstants.HORIZONTAL) / m_LocalsInstance.GetDoofusSpeed() ;
            vTargetPos.z += Input.GetAxis(GameConstants.VERTICAL) / m_LocalsInstance.GetDoofusSpeed();
            this.transform.LookAt(vTargetPos);
            this.transform.position = vTargetPos;

        }
        m_DoofusAnimator.SetInteger(GameConstants.DOOFUS_ANIMATOR_ID, m_uDoofusCurrentState);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Clone"))
        {
            m_uDoofusCurrentState = (int)GameConstants.DOOFUS_CHARACTER_STATE.CELEBRATING;
            m_DoofusAnimator.SetInteger(GameConstants.DOOFUS_ANIMATOR_ID, m_uDoofusCurrentState);          

        }
    }

    IEnumerator DoofusDrop()
    {
        m_LocalsInstance.SetGameOver();
        this.transform.LookAt(m_GMainCamera.transform.position);
        GetComponent<Rigidbody>().useGravity = false;     
        m_uDoofusCurrentState = (int)GameConstants.DOOFUS_CHARACTER_STATE.DROP;
        m_DoofusAnimator.SetInteger(GameConstants.DOOFUS_ANIMATOR_ID, m_uDoofusCurrentState);
        yield return new WaitForSeconds(m_LocalsInstance.GetNewPulpitSpawnTime() - 1);
        GetComponent<Rigidbody>().useGravity = true;
        yield return new WaitForSeconds(m_LocalsInstance.GetNewPulpitSpawnTime() - 1);
        this.transform.position = new Vector3(-100, -100, -100);
    }

   
}
