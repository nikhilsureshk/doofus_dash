using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class GameLocals : MonoBehaviour
{
    Button m_RefreshButton;

    private List<KeyValuePair<float, float>> mPulPitMap ;
    Text _txtDoofusScore;  
    int m_uDoofusScore = 0;
    bool m_bGameOver = false;
    CongifurationData configuration_data;


    private void InitializeRestartButton()
    {
        m_RefreshButton = GameObject.Find("Restart_Button").GetComponent<Button>();
        m_RefreshButton.onClick.AddListener(delegate () { this.ButtonClicked(); });
    }

    private void FetchConfigurationData()
    {       
        using (StreamReader stream = new StreamReader(Application.dataPath + "/Resources/Configuration.json"))
        {
            string m_Json = stream.ReadToEnd();
            configuration_data = JsonUtility.FromJson<CongifurationData>(m_Json);
            Debug.Log(configuration_data);
        }
    }

  

    void Start()
    {
        mPulPitMap = new List<KeyValuePair<float, float>>();
        _txtDoofusScore = GameObject.Find("Doofus_Score").GetComponent<Text>();
        FetchConfigurationData();
        InitializeRestartButton();       
    }

    public void ButtonClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void SetGameOver()
    {
        m_bGameOver = true;        
    }

    public void UpdateDoofusScore()
    {
        _txtDoofusScore.text = m_uDoofusScore.ToString();
        m_uDoofusScore++;        
    }
   
    public void AddNewPulpit(ref GameObject initPulPit, ref List<KeyValuePair<float, float>> mPossiblePositionsList)
    {      
        mPulPitMap.Add(new KeyValuePair<float, float>(transform.position.x, initPulPit.transform.position.z));

        KeyValuePair<float, float> _coordinates = GetNextRandomPulpitPosition(ref mPossiblePositionsList);
        Vector3 targerPos = new Vector3(_coordinates.Key, initPulPit.transform.position.y, _coordinates.Value);
        Instantiate(initPulPit, targerPos, Quaternion.identity);
    }

    KeyValuePair<float, float> GetNextRandomPulpitPosition(ref List<KeyValuePair<float, float>> mPossiblePositionsList)
    {        
        KeyValuePair<float, float> _coordinates;
        while (mPossiblePositionsList.Count > 0)
        {
            int randomVal = Random.Range(0, mPossiblePositionsList.Count);
            _coordinates = mPossiblePositionsList[randomVal];
            
            if (!(mPulPitMap.Contains(_coordinates)))        
                return _coordinates;
            
            mPossiblePositionsList.Remove(_coordinates);
        }
        return _coordinates; 
    }

    public float GetNewPulpitSpawnTime()
    {
        return configuration_data.pulpit_data.pulpit_spawn_time;
    }
    public int GetDoofusSpeed()
    {
        return configuration_data.player_data.speed;
    }
    public int GetPulpitMinLifeTime()
    {
        return configuration_data.pulpit_data.min_pulpit_destroy_time;
    }
    public int GetPulpitMaxLifeTime()
    {
        return configuration_data.pulpit_data.max_pulpit_destroy_time;
    }
    public bool isGameOver()
    {
        return m_bGameOver;
    }



}
